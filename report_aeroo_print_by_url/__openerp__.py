# -*- coding: utf-8 -*-
{
    'name': 'Print Aeroo Reports by URL',
    'version': '1.0',
    'category': 'Report',
    'author':  'Jamotion GmbH',
    'website': 'https://jamotion.ch',
    'summary': 'Print Aeroo Reports directly from /report/pdf/<report name>/<id> URL',
    'images': ['static/description/screenshot.png'],
    'depends': [
        'report',
        'report_aeroo',
    ],
    'demo': [],
    'test': [],
    'application': False,
    'active': False,
    'installable': True,
}
